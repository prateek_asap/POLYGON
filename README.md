# POLYGON 
     Reads csv file that consists of coordinates of edges for 2 polygons in pixels and exposes certain info.

# TO USE
    - Clone the repo & cd into directory.
    - run npm install to install the required packages 
    - npm start
    - localhost:8080 on your browser
